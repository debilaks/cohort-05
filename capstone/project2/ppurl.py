# This is the class we want to test
import requests
import os

def url_exists(url):
    url = os.environ['WORDPRESS_URL']
    user = os.environ['WORDPRESS_USERNAME']
    password = os.environ['WORDPRESS_PASSWORD']
    request = requests.get(url, timeout=10)
    if request.status_code == 200:
        print("Success")
        return True
    elif request.status_code == 404:
        print("Failed")
        return False
