import unittest
from pyblog import Pyblogparser

class TestPyblogparser(unittest.TestCase):
    def test_parser(self):
        argv1 = ['field1']
        parser = Pyblogparser('name').create_parser(argv1)
        self.assertEquals(parser.field1,'read')

if __name__ == '__main__':
    main()
