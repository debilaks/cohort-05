import requests
from requests.auth import HTTPBasicAuth
import sys
import datetime
import os
import argparse

class Pyblogparser:
    def __init__(self,name):
        self.name = name

    def create_parser(args):
        parser = argparse.ArgumentParser()
        parser.add_argument("field1")
        parser.add_argument("-f", type=str, dest="filename")
        return parser.parse_args()

def main():
    getCurrentDateTime = datetime.datetime.now()
    date = (getCurrentDateTime.strftime("%Y-%m-%dT%H:%M:%S"))
    url = os.environ['WORDPRESS_URL']
    user = os.environ['WORDPRESS_USERNAME']
    password = os.environ['WORDPRESS_PASSWORD']
    response = requests.get(url, auth=HTTPBasicAuth(user, password))
    posts = 1
    postcount = 1

    args = Pyblogparser.create_parser(sys.argv[1:])
    if args.field1 == 'read':
        for post in response.json():
            if postcount <= posts:
                title = str(post['title'])
                content = str(post['content'])
                print('Date:', post['date'], "\n" 'Title:', title[13:-1],
                  "\n" 'Content:', content[13:-21])
                postcount = postcount + 1
    elif args.field1 == 'upload' and args.filename == '-':
        data = sys.stdin
        linecnt=0
        contentlist=[]
        for line in data:
           if linecnt == 0:
                title = line.rstrip()
           if line.rstrip()  == 'exit':
               break
           contentlist.append(line.rstrip())
           linecnt += 1
        content = '\n'. join([str(elem) for elem in contentlist])
        post = {
           'title': title,
           'status': 'publish',
           'content': content,
           'date': date
           }   
        response = requests.post(url, auth=HTTPBasicAuth(user, password),
                             json=post)
        print(response)
    elif args.field1 == 'upload' and args.filename != 'None':
        try:
            with open(args.filename, 'r') as f:
               lines = f.read()
               title = lines.split('\n', 1)[0]
               content = lines.split('\n', 1)[1]
            post = {
               'title': title,
               'status': 'publish',
               'content': content,
               'date': date
               }
            response = requests.post(url, auth=HTTPBasicAuth(user, password),
                                 json=post)
            print(response)
        except:
            print("File Not Exist")
    else:
        print("Example Usage:")
        print("python3 pyblog.py read")
        print("- Outputs the contents of the latest blog post to stdout")
        print("python3 pyblog.py upload -f <filename>")
        print("- Uploads the contents of the specified file as a new blog post")
        print(" ")

if __name__ == '__main__':
    main()
