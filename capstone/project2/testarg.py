import fileinput
import sys
import argparse

parse = argparse.ArgumentParser()
group = parse.add_mutually_exclusive_group()
group.add_argument('--read', action='store_true')
group.add_argument('--upload', action='store_true')
parse.add_argument("-f")
args = parse.parse_args()
parse.print_help()
