import requests
from requests.auth import HTTPBasicAuth
import sys
import datetime
import os
import argparse

getCurrentDateTime = datetime.datetime.now()
date = (getCurrentDateTime.strftime("%Y-%m-%dT%H:%M:%S"))
url = os.environ['WORDPRESS_URL']
user = os.environ['WORDPRESS_USERNAME']
password = os.environ['WORDPRESS_PASSWORD']

try:
    request = requests.get(url, timeout=10)
    if request.status_code == 200:
        print('Web site exists')
    else:
        print('Web site does not exist')
except requests.ConnectionError:
    print("Connection Error")
