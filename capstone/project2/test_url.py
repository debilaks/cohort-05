from unittest import TestCase
from unittest.mock import patch
from ppurl import url_exists


class FetchTests(TestCase):
    def test_returns_true_if_url_found(self):
        with patch('requests.get') as mock_request:
            url = 'http://google.com'

            # set a `status_code` attribute on the mock object
            # with value 200
            mock_request.return_value.status_code = 200

            self.assertTrue(url_exists(url))

    def test_returns_false_if_url_not_found(self):
        with patch('requests.get') as mock_request:
                url = 'http://google.com/nonexistingurl'
                #url = 'http://google.com'

                # set a `status_code` attribute on the mock object
                # with value 404
                mock_request.return_value.status_code = 404

                self.assertFalse(url_exists(url))
