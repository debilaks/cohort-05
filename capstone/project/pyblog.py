import requests
from requests.auth import HTTPBasicAuth
import sys
import datetime
import os

getCurrentDateTime = datetime.datetime.now()
date = (getCurrentDateTime.strftime("%Y-%m-%dT%H:%M:%S"))
url = os.environ['WORDPRESS_URL']
user = os.environ['WORDPRESS_USERNAME']
password = os.environ['WORDPRESS_PASSWORD']

response = requests.get(url, auth=HTTPBasicAuth(user, password))
posts = 1
postcount = 1
if sys.argv[1] == 'read':
    for post in response.json():
        if postcount <= posts:
            title = str(post['title'])
            content = str(post['content'])
            print('Date:', post['date'], "\n" 'Title:', title[13:-1],
                  "\n" 'Content:', content[13:-21])
            postcount = postcount + 1
elif sys.argv[1] == 'upload' and sys.argv[2] == '-f' and sys.argv[3] != '-':
    try:
        with open(sys.argv[3], 'r') as f:
           lines = f.read()
           title = lines.split('\n', 1)[0]
           content = lines.split('\n', 1)[1]
        post = {
           'title': title,
           'status': 'publish',
           'content': content,
           'date': date
           }
        response = requests.post(url, auth=HTTPBasicAuth(user, password),
                             json=post)
        print(response)
    except:
        print("File Not Exist")
elif sys.argv[1] == 'upload' and sys.argv[2] == '-f' and sys.argv[3] == '-':
    data = sys.stdin
    linecnt=0
    contentlist=[]
    for line in data:
       if linecnt == 0:
            title = line.rstrip()
       if line.rstrip()  == 'exit':
           break
       contentlist.append(line.rstrip())
       linecnt += 1
    content = '\n'. join([str(elem) for elem in contentlist])
    post = {
       'title': title,
       'status': 'publish',
       'content': content,
       'date': date
       }
    response = requests.post(url, auth=HTTPBasicAuth(user, password),
                             json=post)
    print(response)
else:
    print("Example Usage:")
    print("python3 pyblog.py read")
    print("- Outputs the contents of the latest blog post to stdout")
    print("python3 pyblog.py upload -f <filename>")
    print("- Uploads the contents of the specified file as a new blog post")
    print(" ")
