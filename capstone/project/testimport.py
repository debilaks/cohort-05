import fileinput
import sys

data = []
for line in fileinput.input():
    if line.rstrip() == 'exit':
        break
    data.append(line.rstrip())
print(*data, sep = "\n")
