#!/bin/bash

echo $1
filename="$1"
if [[  -f  ${filename} ]]; then
	echo "File Name exist"
elif [[ -d ${filename} ]]; then
	echo "Its directory"
else
	echo "File or directory doesn't exist"
fi

# Change filename extensions

if [[ $# -ne 2 ]]; then
   echo "Need exactly 2 arguments">&2
   exit 1
fi

for  f in $@; do
    echo "f valiue is:" $f
    if [[  -f  ${f} ]]; then
        echo "File Name exist"
	modfilename=$f"_inspectedbyDJ"
        echo "modified filename is:" $modfilename
	mv $f $modfilename
     elif [[ -d ${f} ]]; then
        echo "Its directory" 
	cd $f
    else
        echo "File or directory doesn't exist"
    fi
#    base=$(basename "$f" "$1")
#    echo "base value is: " $base
#    mv "$f" "${base}$2"
done
