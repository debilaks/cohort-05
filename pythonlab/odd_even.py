anynum = input("Enter a number: ")
if type(int(anynum)) != int:
    print("Please enter valid integer")
else:
   anynum = int(anynum)
   if anynum % 2 == 0:
       print("The number you entered", anynum, "is even number")
   else:
       print("The number you entered", anynum, "is odd number")
