import json
import requests

# getting data from source
get_data = requests.get("https://reqres.in/api/users?page=2")
# pretty printing data
pretty_data = json.dumps(get_data.json(), indent=4)
print(pretty_data)
