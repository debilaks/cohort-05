#!/usr/bin/env python3
#Use a list comprehension to create a list of the squares of the integers from 1 to 25 (i.e, 1, 4, 9, 16, …, 625)

integer_list = list(range(1,26))
print(integer_list)

square_list = [num*num for num in integer_list]
print(square_list)
