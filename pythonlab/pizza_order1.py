
def find_order(choice1, choice2):
    if choice1 == 1 and choice2 == 1:
        user_order = 'cs'
    elif choice1 == 1 and choice2 == 2:
        user_order = 'cm'
    elif choice1 ==  1 and choice2 == 3:
        user_order = 'cl'
    elif choice1 == 2 and choice2 == 1:
        user_order = 'ots'
    elif choice1 == 2 and choice2 == 2:
        user_order = 'otm'
    elif choice1 ==  2 and choice2 == 3:
        user_order = 'otl'
    elif choice1 == 3 and choice2 == 1:
        user_order = 'tts'
    elif choice1 == 3 and choice2 == 2:
        user_order = 'ttm'
    elif choice1 ==  3 and choice2 == 3:
       user_order = 'ttl'
    else:
       user_order = 'none'
    return(user_order)

food_menu = {}
food_menu = {'Pizza': ['Cheese', '2toppings', '3toppings']}
size = ['large', 'medium', 'small']
price = {'cl': 4.99,
        'cm': 3.99,
        'cs': 1.99,
        'otl': 5.99,
        'otm': 4.99,
        'ots': 2.99,
        'ttl': 6.99,
        'ttm': 5.99,
        'tts': 3.99
        }

order = {'cl': "Large, Cheese Pizza",
        'cm': "Medium, Cheese Pizza",
        'cs': "Small, Cheese Pizza",
        'otl': "Large, One Topping Pizza",
        'otm': "Medium, One Topping Pizza",
        'ots': "Small, One Topping Pizza",
        'ttl': "Large, Two Topping Pizza",
        'ttm': "Medium, Two Topping Pizza",
        'tts': "Small, Two Topping Pizza"
        }


#for item in food_menu:
#    print(item)
 #   print(food_menu[item])
 #   for x in food_menu[item]:
  #      print(x)

#for pz in price:
#    print(pz, price[pz])

print("Enter Your Choice of Pizza: \n\
                  1: cheese\n\
                  2: one topping\n\
                  3: two topping\n ")

choice1 = int(input("Enter:"))

print("Enter Size of Pizza: \n\
                  1: small\n\
                  2: medium\n\
                  3: large\n ")

choice2 = int(input("Enter:"))
qty = int(input("Enter Quantity:"))

order_code = find_order(choice1, choice2)
print(order_code)

total_amount = round(qty * price[order_code],2)
#print(total_amount)
#print(user_order)

print("You have ordered: ",qty, order[order_code])
print ("Your Total amount: $", total_amount)

