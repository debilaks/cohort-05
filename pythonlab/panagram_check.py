#write a function to check whether its string argument is a pangram 
#(i.e., it contains all of the letters of the alphabet)
def remove_duplicate(duplist):
    duplist.sort()
    output_list=[]
    prev_val=0
    index=0
    for curr_val in duplist:
        if curr_val != prev_val:
            prev_val = curr_val
            output_list.append(curr_val)
    return output_list

def is_panagram(strname):
    ALPHABET = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z'
    alpha_cnt= 0
    ret_val = ''
    strlist = []
    #remove space and convert into list
    strname=strname.replace(" ", "") 
    strlist[:0] = strname
    strlist.sort()
#    print(strlist)
    sort_strname=remove_duplicate(strlist)
    for s in sort_strname:
        if s in ALPHABET:
            alpha_cnt += 1
            #print(s)
#    print(alpha_cnt)
    if alpha_cnt == 26:
        ret_val = "yes"
 #       print(ret_val)
    else:
        ret_val = "no"
#        print(ret_val)
    return ret_val

#input_string = 'hello how are you'
input_string = 'a quick brown fox jumps over the lazy dog'
status = is_panagram(input_string)
if status == 'yes':
    print("Give string is panagram")
else:
    print("Given string is not panagram")
