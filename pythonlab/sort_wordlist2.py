import builtins
#input_word = "England"
word = input("Enter a word: OR quit: \n")
input_word=builtins.list(word)

vowel=['a','e','i','o','u','A','E','I','O','U']
vowel_list=[]
consonant_list=[]
while word != 'quit':
    if input_word[0] in vowel:
        vowel_list.append(''.join(input_word))
    else:
        consonant_list.append(''.join(input_word))
    word = input("Enter a word OR quit: \n")
    input_word=builtins.list(word)
    
vowel_list.sort()
print("***vowel list***\n",vowel_list)

#consonant_list.sort()
print("\n***consonant list***\n",sorted(consonant_list))
