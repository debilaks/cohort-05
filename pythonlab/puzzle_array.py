import numpy as nmp
X = nmp.array( [ [ 1, 6, 7],
[ 5, 9, 2],
[ 3, 8, 4] ] )
print(X[1][2]) # element at the given index i.e. 2
print(X[0])     # first row
print(X[1])      # second row
print(X[-1])     # last row
print(X[:, 0])  # first column
print(X[:, 2])  # third column
print(X[:, -1]) # last column
