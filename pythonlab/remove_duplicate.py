#write a function which accepts a list as its argument 
#and returns a new list with all of the duplicates removed 
#(e.g., remove_dupes([3, 1, 2, 3, 1, 3, 3, 4, 1]) would return [3, 1, 2, 4])

def remove_duplicate(duplist):
    duplist.sort()
    output_list=[]
    prev_val=0
    index=0
    for curr_val in duplist:
        if curr_val != prev_val:
            prev_val = curr_val
            output_list.append(curr_val)
    return output_list
        
duplist = [10,20,30,10,78]
duplist = [3, 1, 2, 3, 1, 3, 3, 4, 1]
unique_list = remove_duplicate(duplist)
print(unique_list)
