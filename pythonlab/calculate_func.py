#Write a function calculate which is passed two operands and an operator 
#and returns the calculated result, e.g., calculate(2, 4, '+') would return 6
import math

def calculate(val1, val2, oper):
    try:
        val1 = int(val1)
        val2 = int(val2)
        result=0
    except ValueError:
        print("Outer Exception")
        result = None
        return result
    
    if oper == '+':
        result = val1 + val2
    elif oper == '-':
        result = val1 - val2
    elif oper == '*':
        result = val1 * val2
    elif oper == '/':
        try:
            result = val1 / val2
        except ZeroDivisionError:
            print('ZeroDivisionError!')
            result = None
        except Exception as other:
            print("You cannot divide by zero !", other)
            #raise
            result = None
    elif oper == 'log':
        try:
            result = math.log(val1,val2)
        except Exception as other:
             print("log operator error", other)           
    return result
