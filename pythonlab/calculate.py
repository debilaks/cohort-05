#Write a function calculate which is passed two operands and an operator 
#and returns the calculated result, e.g., calculate(2, 4, '+') would return 6
import calculate_func
import sys

print(sys.argv[0])
print(sys.argv[1])
print(sys.argv[2])
try:
    x = int(sys.argv[1])
    y = int(sys.argv[2])
    oper = sys.argv[3]
    value = calculate_func.calculate(x,y,oper)
    print(x, oper, y, "=", value)

except ValueError:
    print("Please pass integer arguments")


#addvalue = calculate(x,y,'+')
#subvalue = calculate(x,y,'-')
#mulvalue = calculate(x,y,'*')
#divvalue = calculate(x,y,'/')
#logvalue = calculate(x,y,'log')
#print("Addition of ", x, "and", y, "=", addvalue)
#print("Subtraction of ", x, "and", y, "=", subvalue)
#print("Multiplication of ", x, "and", y, "=", mulvalue)
#print("Division of ", x, "and", y, "=", divvalue)
#print("Log of ", x, "and", y, "=", logvalue)
