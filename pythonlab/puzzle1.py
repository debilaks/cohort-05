import random, math

def surroundingPieces(z, w, h):
    x = z%w
    y = math.floor(z/h)

    left  = None if x == 0 else z - 1
    up    = None if y == 0 else z - w
    right = None if x == (w-1) else z + 1
    down  = None if y == (h-1) else z + w

    return list(filter(None, [left, up, right, down]))

def shuffle(puzzle, moves, width, height):
    empty = puzzle.index(0)     #find initial empty index
    lastPiece = empty           #init lastPiece
    solution  = [empty]         #init solution
    print("puzzle", puzzle, "moves", moves, "width", width, "height", height)

    for _ in range(moves):
        #get surrounding pieces
        pieces = surroundingPieces(empty, width, height)

        #remove the last piece we moved
        if lastPiece in pieces:
            pieces.remove(lastPiece)

        #select a piece
        pieceIndex = random.choice(pieces)
        solution.insert(0, pieceIndex) #insert move in solution

        #swap        
        puzzle[empty] = puzzle[pieceIndex]
        lastPiece = empty  #store this piece index

        puzzle[pieceIndex] = 0
        empty = pieceIndex #store new empty index

    return puzzle, solution

    
#this is the program equivalent of manually solving the puzzle
def solve(pzl, sol):
    print("pzl", pzl,"sol", sol)
    for i in range(len(sol)-1):
        pzl[sol[i]]   = pzl[sol[i+1]]
        pzl[sol[i+1]] = 0
        
    return pzl

puzzle, solution = shuffle([1, 2, 3, 4, 0, 5, 6, 7, 8], 10, 3, 3)

print(puzzle)                       #[1, 3, 0, 7, 2, 6, 4, 8, 5]     
print(solution)                     #[2, 1, 4, 5, 8, 7, 4, 3, 6, 7, 4]
print(solve(puzzle[:], solution))   #[1, 2, 3, 4, 0, 5, 6, 7, 8]

