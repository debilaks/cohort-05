#!/usr/bin/env python3
# https://www.simplifiedpython.net/openweathermap-api-python/
import requests
 
from pprint import pprint
API_key = "0d8c876f42ae134bce76290f7bc911d7"

base_url = "http://api.openweathermap.org/data/2.5/weather?"
 
city_name = input("Enter a city Name : ")
Final_url = base_url + "appid=" + API_key + "&q=" + city_name
 
weather_data = requests.get(Final_url).json()
print("\nCurrent Weather Data Of " + city_name +":\n")
pprint(weather_data)

temp = weather_data['main']['temp']

wind_speed = weather_data['wind']['speed']

description = weather_data['weather'][0]['description']

latitude = weather_data['coord']['lat']

longitude = weather_data['coord']['lon']

print('\nTemperature : ',temp)
print('\nWind speed  : ',wind_speed)
print('\nDescription : ',description)
print('\nLatitude    : ',latitude)
print('\nLongtitude  : ',longitude)



