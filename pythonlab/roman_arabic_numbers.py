roman_to_arabic = {'M':1000, 'D':500, 'C':100, 'L':50, 'X':10, 'V':5, 'I':1}
roman_num='MDCLXVI'
#roman_num='IX'
roman_list=list(roman_num)
#print(roman_list)

#for key, value in roman_to_arabic.items():
#    print(key, '->', value)
arabic_num=0
prev_num=0
curr_num=0
# Read in reverse order
for num in roman_num[::-1]:
    curr_num = int(roman_to_arabic[num])
#    print("curr",curr_num)
#    print("prev",prev_num)

    if prev_num < curr_num: # If previous value is less add
        arabic_num += curr_num * 1   
    elif prev_num >= curr_num: # If previous value is greater subtract
        arabic_num += curr_num * -1

    prev_num= curr_num
print("Roman Number ", roman_num, "\nArabic Number ", arabic_num)
