import random
city_list = ['brockton', 'chicago', 'miami', 'boston', 'easton', 'canton']
# Read a file and store the content in the list
# Also remove eol character \n
with open('wordlist.txt') as file:
    city_list=file.read().splitlines()
#print(city_list)

# Get the random word from the entire list
rand_word =random.choice(city_list)
#print(rand_word)

# Shuffule the random word
shuffled_word = list(rand_word)
random.shuffle(shuffled_word)
#print(shuffled_word)

#intialize the loop, atleast play one time
user_word=''
pstat = 'y'
cnt=0
ntry = 4
while pstat == 'y':
 #   print(rand_word)
    print("Solve the given scrambled word:", ''.join(shuffled_word))
    user_word=input("\nEnter your word:")
    cnt=0
    uchoice='n'
    while rand_word != user_word:
 #       print(rand_word)
        cnt += 1
        if cnt >= ntry:
            print("Your execeeded", ntry, " tries, Game over:\n Correct Word is:", rand_word)  
            break
        elif (cnt ==2 and cnt != ntry):
            print("Hint: first 3 char:", rand_word[:3])
        else:
            user_word=input("\nIts not correct word\nEnter your word:")
    if cnt >= ntry:
        break
    print("Well done: ", rand_word, "is correct word")
    pstat = input("Would you like to play again:y/n:")
    rand_word =random.choice(city_list)
    shuffled_word = list(rand_word)
    random.shuffle(shuffled_word)

#print(random.shuffle(city_list))
#print(city_list)

