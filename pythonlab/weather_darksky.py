#!/usr/bin/env python3
import requests

from pprint import pprint

def get_weather():
    SECRET_KEY = '1d8c58ed1d54f96f939e706c788650f1'
    city_name = 'Boston'
    lat, long = (42.3603,-71.0573)  # Boston, MA
    url = 'https://api.darksky.net/forecast/{key}/{lat},{long}'.format(
          key=SECRET_KEY, lat=lat, long=long)
    weather_data = requests.get(url).json()

    print("\nCurrent Weather Data Of " + city_name +":\n")
    time = weather_data['currently']['time']
    temp = weather_data['currently']['temperature']
    print(time, temp)
    today = weather_data['daily']['data'][0]

    print('Today - High: {high}, Low: {low}'.format(
        high=today['temperatureHigh'], low=today['temperatureLow']))

    return None

get_weather()

