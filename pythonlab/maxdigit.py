#write max3, a function to find the maximum of three values (
# (first create a function that finds the maximum of two values and have max3 call it)
def max2(x,y):
    if x > y:
        maxval2 = x
    else:
        maxval2 = y
    return maxval2

def max3(x,y,z):
    res2 = max2(x,y)
    maxval3 = max2(z, res2)
    return maxval3

x, y, z = 100, 25, 15
#print(x,y)
maximum = max3(x, y,z)
print("Maximum of ", x, ",", y, ",", z, "is:", maximum)
