# Leibniz's formula
# Formula X = 4 - 4/3 + 4/5 - 4/5 + 4/9
import math
#print ("pi from math library is:", math.pi)
pi = 0
numerator = 4
denominator = 1
n = int(input("Enter Number of iterations Below: "))
#print (1%2)
#print (2%2)
#print (3%2)
#print (4%2)

#print ((2 * (1%2)) -1)
#print ((2 * (2%2)) -1)

for i in range(1,n):
    a = 2 * (i%2) -1
    pi += a * (numerator / denominator)
    denominator += 2
    #print(denominator)
#print("Number Of Iterations:", n)
print ("pi from math library is:", math.pi)
print ("pi from iteration", n, "is:",pi)

