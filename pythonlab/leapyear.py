year = int(input("Enter a Year: "))

if (year % 4 == 0): 
    if (year % 400 == 0):
        print(year, "was leap year")
        if (year % 100 != 0):
            print(year, "was leap year")
    else:
        print(year, "was not leap year")
else:
    print(year, "was not leap year")
