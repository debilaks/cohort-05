#write a function to sum all of the numbers in a list
def sum_list(num_list):
    sumval=0
    for x in num_list:
        sumval += int(x)
    return sumval
        
num_list = [10,20,30,40,125,78]
print(num_list)
result = sum_list(num_list)
print(result)
